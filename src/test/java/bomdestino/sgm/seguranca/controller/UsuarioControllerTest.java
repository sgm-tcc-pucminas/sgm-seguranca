package bomdestino.sgm.seguranca.controller;

import bomdestino.sgm.seguranca.MockitoExtension;
import bomdestino.sgm.seguranca.model.dto.UsuarioDto;
import bomdestino.sgm.seguranca.service.UsuarioService;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UsuarioControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private UsuarioController controller;

    @Mock
    private UsuarioService service;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void obterUsuarioPorIdTest() {
        Long idUsuario = 1L;
        String nomeUsuario = "Solzanir Souza";

        when(service.obterPorId(idUsuario))
                .thenReturn(Optional.of(new UsuarioDto()
                        .setId(idUsuario)
                        .setNome(nomeUsuario)));

        controller.obterPorId(1L);

        given()
                .standaloneSetup(controller)
                .contentType(ContentType.JSON)
                .get("/api/private/v1/usuario/" + idUsuario)
                .then().log().all()
                .statusCode(200)
                .assertThat()
                .body("nome", equalTo(nomeUsuario))
                .body("id", equalTo(idUsuario.intValue()));
    }
}
