package bomdestino.sgm.seguranca;

import bomdestino.sgm.seguranca.model.document.UsuarioDocument;
import bomdestino.sgm.seguranca.repository.UsuarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import static bomdestino.sgm.seguranca.util.enums.PermissaoUsuarioEnum.ADMINISTRATIVE;
import static bomdestino.sgm.seguranca.util.enums.PermissaoUsuarioEnum.GENERAL;
import static bomdestino.sgm.seguranca.util.enums.PermissaoUsuarioEnum.SISTEMIC;
import static java.util.Arrays.asList;

@SpringBootApplication
public class SgmSegurancaApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(SgmSegurancaApplication.class);

    @Autowired
    private UsuarioRepository usuarioRepository;

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SgmSegurancaApplication.class, args);
        ConfigurableEnvironment environment = run.getEnvironment();

        log.info("\n\n\t------------------------------------------------------------\n\t" +
                ":: SGM Seguranca inicializado na porta: " + environment.getProperty("server.port") + " ::" +
                "\n\t------------------------------------------------------------\n\t");
    }

    @Override
    public void run(String... args) {
        if (!usuarioRepository.findById(0L).isPresent()) {
            usuarioRepository.save(new UsuarioDocument()
                    .setId(0L)
                    .setUsername("sistemico")
                    .setCpf("987.654.321-0")
                    .setNome("Usuario Sistemico")
                    .setTelefone("(19) 98765-4321")
                    .setEmail("suporte@bomdestino.sp.gov.br")
                    .setCep(39888000)
                    .setSenha("abc123")
                    .setPermissions(asList(SISTEMIC.toString())));
        }

        if (!usuarioRepository.findById(1L).isPresent()) {
            usuarioRepository.save(new UsuarioDocument()
                    .setId(1L)
                    .setUsername("administrativo")
                    .setCpf("987.654.321-1")
                    .setNome("Usuário Administrativo")
                    .setTelefone("(19) 98765-4321")
                    .setEmail("administrativo@bomdestino.sp.gov.br")
                    .setCep(39888000)
                    .setSenha("abc123")
                    .setPermissions(asList(ADMINISTRATIVE.toString())));
        }

        if (!usuarioRepository.findById(2L).isPresent()) {
            usuarioRepository.save(new UsuarioDocument()
                    .setId(2L)
                    .setUsername("usuario")
                    .setCpf("987.654.321-2")
                    .setNome("Usuário Comum")
                    .setTelefone("(19) 98765-4321")
                    .setEmail("usuario@bomdestino.sp.gov.br")
                    .setCep(39888000)
                    .setSenha("abc123")
                    .setPermissions(asList(GENERAL.toString())));
        }
    }
}
