package bomdestino.sgm.seguranca.controller;

import bomdestino.sgm.seguranca.model.dto.UsuarioDto;
import bomdestino.sgm.seguranca.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/private/")
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @GetMapping("/v1/usuario/{id}")
    public ResponseEntity<UsuarioDto> obterPorId(@PathVariable("id") Long idUsuario) {
        Optional<UsuarioDto> usuarioOpt = service.obterPorId(idUsuario);

        if (usuarioOpt.isPresent()) {
            return ResponseEntity.ok().body(usuarioOpt.get());

        } else {
            return ResponseEntity.noContent().build();
        }
    }
}