package bomdestino.sgm.seguranca.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

import static bomdestino.sgm.seguranca.util.enums.PermissaoUsuarioEnum.ADMINISTRATIVE;
import static bomdestino.sgm.seguranca.util.enums.PermissaoUsuarioEnum.SISTEMIC;
import static bomdestino.sgm.seguranca.util.enums.PermissaoUsuarioEnum.SUPORT;
import static java.lang.String.format;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private static final String RESOURCE_ID = "rokin-application";
    private static final String ROOT_PATTERN_PRIVATE = "/api/private/**";
    private static final String ROOT_PATTERN_ADMIN = "/api/administrative/**";

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(RESOURCE_ID);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/swagger-ui.html", "/swagger-resources/**", "/api/public/**", "/oauth/token").permitAll()
                .anyRequest().authenticated();

        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, ROOT_PATTERN_PRIVATE).access(obterAcessoPrivado("read"))
                .antMatchers(HttpMethod.POST, ROOT_PATTERN_PRIVATE).access(obterAcessoPrivado("write"))
                .antMatchers(HttpMethod.PUT, ROOT_PATTERN_PRIVATE).access(obterAcessoPrivado("write"))
                .antMatchers(HttpMethod.PATCH, ROOT_PATTERN_PRIVATE).access(obterAcessoPrivado("read"))
                .antMatchers(HttpMethod.DELETE, ROOT_PATTERN_PRIVATE).access(obterAcessoPrivado("write"));

        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, ROOT_PATTERN_ADMIN).access(obterAcessoAdministrative("read"))
                .antMatchers(HttpMethod.POST, ROOT_PATTERN_ADMIN).access(obterAcessoAdministrative("write"))
                .antMatchers(HttpMethod.PUT, ROOT_PATTERN_ADMIN).access(obterAcessoAdministrative("write"))
                .antMatchers(HttpMethod.DELETE, ROOT_PATTERN_ADMIN).access(obterAcessoAdministrative("write"));
    }

    private String obterAcessoPrivado(String escopo) {
        return format("#oauth2.hasScope('%s') and hasAuthority('%s') or hasAuthority('%s')",
                escopo, SISTEMIC, SUPORT);
    }

    private String obterAcessoAdministrative(String escopo) {
        return format("#oauth2.hasScope('%s') and hasAuthority('%s') or hasAuthority('%s')",
                escopo, ADMINISTRATIVE, SUPORT);
    }
}
