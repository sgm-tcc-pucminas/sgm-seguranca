package bomdestino.sgm.seguranca.config;

import bomdestino.sgm.seguranca.model.document.UsuarioDocument;
import bomdestino.sgm.seguranca.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MongoAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken authToken = (UsernamePasswordAuthenticationToken) authentication;

        String auth = authToken.getName();
        String password = authToken.getCredentials().toString();
        Optional<UsuarioDocument> usuarioOpt = usuarioService.obterPorUsername(auth);

        if (!usuarioOpt.isPresent()) {
            throw new AuthenticationCredentialsNotFoundException("Usuário não localizado");
        }

        UsuarioDocument usuario = usuarioOpt.get();

        if (usuario.getSenha().equals(password)) {
            return new UsernamePasswordAuthenticationToken(usuario, password, usuario.buildGrantedAuthorities());

        } else {
            throw new AuthenticationCredentialsNotFoundException("Senha inválida para o usuário informado");
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(aClass);
    }
}
