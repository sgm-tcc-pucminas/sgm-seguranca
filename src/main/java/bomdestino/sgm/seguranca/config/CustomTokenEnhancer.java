package bomdestino.sgm.seguranca.config;

import bomdestino.sgm.seguranca.model.document.UsuarioDocument;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

public class CustomTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

        if (authentication.getPrincipal() instanceof UsuarioDocument) {
            UsuarioDocument usuario = (UsuarioDocument) authentication.getPrincipal();

            final Map<String, Object> additionalInfo = new HashMap<>();
            additionalInfo.put("id", usuario.getId());
            additionalInfo.put("nome", usuario.getNome());
            additionalInfo.put("cpf", usuario.getCpf());
            additionalInfo.put("email", usuario.getEmail());
            additionalInfo.put("cep", usuario.getCep());
            additionalInfo.put("senha", usuario.getSenha());
            additionalInfo.put("authorities", usuario.getPermissions());
            ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        }

        return accessToken;
    }
}
