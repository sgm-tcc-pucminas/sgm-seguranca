package bomdestino.sgm.seguranca.model.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

@Document(collection = "usuario")
public class UsuarioDocument implements Serializable  {

    @Id
    private Long id;
    private String username;
    private String cpf;
    private String nome;
    private String email;
    private String telefone;
    private Integer cep;
    private String senha;
    private List<String> permissions;

    public Long getId() {
        return id;
    }

    public UsuarioDocument setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UsuarioDocument setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getCpf() {
        return cpf;
    }

    public UsuarioDocument setCpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public UsuarioDocument setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UsuarioDocument setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getTelefone() {
        return telefone;
    }

    public UsuarioDocument setTelefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public Integer getCep() {
        return cep;
    }

    public UsuarioDocument setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    public String getSenha() {
        return senha;
    }

    public UsuarioDocument setSenha(String senha) {
        this.senha = senha;
        return this;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public UsuarioDocument setPermissions(List<String> permissions) {
        this.permissions = permissions;
        return this;
    }

    public List<GrantedAuthority> buildGrantedAuthorities() {
        if (isNull(this.getPermissions())) {
            return getPermissions().stream()
                    .map(a -> new SimpleGrantedAuthority(a))
                    .collect(toList());
        }

        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsuarioDocument that = (UsuarioDocument) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
