package bomdestino.sgm.seguranca.model;

import bomdestino.sgm.seguranca.model.document.UsuarioDocument;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class Usuario extends UsuarioDocument implements UserDetails {

    public Usuario() {
    }

    public Usuario(UsuarioDocument user) {
        this.setId(user.getId());
        this.setNome(user.getNome());
        this.setEmail(user.getEmail());
        this.setCep(user.getCep());
        this.setCpf(user.getCpf());
        this.setPermissions(user.getPermissions());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return buildGrantedAuthorities();
    }

    @Override
    public String getPassword() {
        return getSenha();
    }

    @Override
    public String getUsername() {
        return getCpf();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
