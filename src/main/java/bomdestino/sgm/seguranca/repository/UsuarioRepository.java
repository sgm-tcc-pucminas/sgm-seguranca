package bomdestino.sgm.seguranca.repository;

import bomdestino.sgm.seguranca.model.document.UsuarioDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends MongoRepository<UsuarioDocument, Long> {

    Optional<UsuarioDocument> findByUsername(String cpf);
}
