package bomdestino.sgm.seguranca.service;

import bomdestino.sgm.seguranca.model.Usuario;
import bomdestino.sgm.seguranca.model.document.UsuarioDocument;
import bomdestino.sgm.seguranca.model.dto.UsuarioDto;
import bomdestino.sgm.seguranca.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UsuarioDocument usuario = obterPorUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Usuáio não localizado"));

        return new Usuario(usuario);
    }

    public Optional<UsuarioDocument> obterPorUsername(String username) {
        return repository.findByUsername(username);
    }

    public Optional<UsuarioDto> obterPorId(Long idUsuario) {
        Optional<UsuarioDocument> usuarioOpt = repository.findById(idUsuario);

        if (usuarioOpt.isPresent()) {
            UsuarioDocument usuario = usuarioOpt.get();

            return Optional.of(new UsuarioDto()
                    .setId(usuario.getId())
                    .setNome(usuario.getNome())
                    .setEmail(usuario.getEmail())
                    .setTelefone(usuario.getTelefone()));

        } else {
            return Optional.empty();
        }
    }
}
